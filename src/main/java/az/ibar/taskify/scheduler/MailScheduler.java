package az.ibar.taskify.scheduler;

import az.ibar.taskify.dao.entity.Task;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.TaskRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.enums.MailSendStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableScheduling
@Slf4j
public class MailScheduler {

    private JavaMailSender javaMailSender;
    private TaskRepository taskRepository;
    private UserRepository userRepository;

    public MailScheduler(JavaMailSender javaMailSender, TaskRepository taskRepository, UserRepository userRepository) {
        this.javaMailSender = javaMailSender;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Scheduled(fixedDelay = 10000)
    void sendEmail() {

        List<Task> tasks = taskRepository.findBySendStatus(MailSendStatus.NOT_SENDED);


        tasks.forEach(task -> {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setFrom("azimlishakir@gmail.com");
            msg.setSubject(task.getTitle());
            msg.setText(task.getDescription()+"--"+task.getDeadline());
            User user = userRepository.findById(task.getAssignId()).orElse(null);
            if (user != null) {
                System.out.println(user.getEmail());
                msg.setTo(user.getEmail());
                javaMailSender.send(msg);
                log.info("Mail successfully sender");
            }

            task.setSendStatus(MailSendStatus.SENDED);
            taskRepository.save(task);
        });

    }

}
