package az.ibar.taskify.exception;

import lombok.Data;

@Data
public class UserNotFoundException extends RuntimeException {

    private String message;
    private String code;

    public UserNotFoundException(String message, String code) {
        this.message = message;
        this.code = code;
    }


}
