package az.ibar.taskify.controller;

import az.ibar.taskify.domain.dto.UserDto;
import az.ibar.taskify.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

@RestController
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/createUser")
    @PreAuthorize("@authServiceImpl.hasValidToken(#token)")
    public ResponseEntity createUser(@RequestBody UserDto userDto,
                                 @RequestHeader("Auth") @NotBlank String token) {
        userService.createUser(userDto, token);
        return ResponseEntity.ok().build();
    }



}
