package az.ibar.taskify.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private List<Long> assignIds;
    private String title;
    private String description;
    private LocalDate deadline;

}
