package az.ibar.taskify.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationDto {

    private String username;

    private String name;

    private String lastName;

    private String phoneNumber;

    private String password;

    private String organizationName;

    private String address;

    private String email;

}
