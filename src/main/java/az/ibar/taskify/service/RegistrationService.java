package az.ibar.taskify.service;

import az.ibar.taskify.domain.dto.RegistrationDto;

public interface RegistrationService {

    void signUp(RegistrationDto registrationDto);
}
