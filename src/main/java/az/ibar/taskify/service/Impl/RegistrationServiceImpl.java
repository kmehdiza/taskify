package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.Role;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.RoleRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.RegistrationDto;
import az.ibar.taskify.mapper.UserMapper;
import az.ibar.taskify.service.RegistrationService;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    public RegistrationServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void signUp(RegistrationDto registrationDto) {
        User user = UserMapper.INSTANCE.toEntity(registrationDto);
        Role role = roleRepository.findByRoleName("ADMIN");
        user.setRole(role);
        userRepository.save(user);
    }
}
