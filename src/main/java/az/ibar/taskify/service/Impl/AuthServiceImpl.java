package az.ibar.taskify.service.Impl;

import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.AuthDto;
import az.ibar.taskify.domain.dto.AuthRespDto;
import az.ibar.taskify.exception.UserNotFoundException;
import az.ibar.taskify.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public AuthRespDto login(AuthDto authDto) {

      User user = userRepository.findByUsernameAndPassword(authDto.getUsername(), authDto.getPassword())
                .orElseThrow(()->new UserNotFoundException("user not found","exception.user-not-found"));

      String token = UUID.randomUUID().toString();
      user.setToken(token);
      userRepository.save(user);

      return AuthRespDto.builder().token(token).build();
    }

    @Override
    public boolean hasValidToken(String token) {
        return userRepository
                .findByToken(token).isPresent();
    }
}
