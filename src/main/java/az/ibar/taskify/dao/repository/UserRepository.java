package az.ibar.taskify.dao.repository;


import az.ibar.taskify.dao.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
   Optional<User> findByUsernameAndPassword(String username, String password);
   Optional<User> findByToken(String token);
   List<User> findAll();
}
