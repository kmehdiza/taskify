package az.ibar.taskify.service;

import az.ibar.taskify.dao.entity.Task;
import az.ibar.taskify.dao.entity.User;
import az.ibar.taskify.dao.repository.TaskRepository;
import az.ibar.taskify.dao.repository.UserRepository;
import az.ibar.taskify.domain.dto.TaskDto;
import az.ibar.taskify.domain.enums.MailSendStatus;
import az.ibar.taskify.mapper.TaskMapper;
import az.ibar.taskify.service.Impl.TaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final MailSendStatus DUMMY_STATUS = MailSendStatus.NOT_SENDED;

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Spy
    private TaskMapper taskMapper;

    private TaskDto taskDto;
    private Task task;
    private User user;

    @BeforeEach
    void setUp(){
        taskDto = TaskDto
                .builder()
                .assignIds(List.of(DUMMY_ID))
                .build();

        user = User
                .builder()
                .id(DUMMY_ID)
                .build();

        task = Task
                .builder()
                .reporterUser(user)
                .assignId(DUMMY_ID)
                .sendStatus(DUMMY_STATUS)
                .build();
    }

    @Test
    void givenValidTokenAndTaskDtoWhenCreateTaskThenSave(){
        //Arrange
        when(userRepository.findByToken(DUMMY_STRING)).thenReturn(Optional.of(user));

        //Act
        taskService.createTask(taskDto,DUMMY_STRING);

        //Verify
        verify(taskRepository,times(1)).save(task);
    }
}
